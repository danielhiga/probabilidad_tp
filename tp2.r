#tp2

#sobre los encuestados
setwd("/home/daniel/Documentos/utn/probabilidad_aplicada/tp/") # fija la ruta de la carpeta donde se encuentran los archivos con los que se va a trabajar y donde se guardarán las salidas que se ejecuten 
getwd() # devuelve la ruta de la carpeta de trabajo
library(grDevices) # Equipos gr?ficos y soporte para la base y la red de gr?ficos
library(tcltk)
library(aplpack)
library(corrplot)
library(ggplot2)
library(plotrix)
library(rgl)
library(tcltk2)


library("xlsx")
library(readxl)
Internet2013 <- read_excel("Internet2013.xlsx")

pdf(file ="out.pdf")
#View(Internet2013)

# significado de los datos
# encuesta a usuarios de distintos paises sobre el uso de internet, autos y el consumo de tabaco 
# durante la semana anterior a la encuesta.
# id: id de registro
# Nacionalidad: pais de nacimiento del usuario
# edad: edad del usuario, en años
# estatura: estatura en metros del usuario
# sitio: la cantidad de sitios navegados (durante la ultima semana)
# uso: tiempo en minutos dedicado a navegar por internet (durante la ultima semana)
# temperatura: temperatura en el lugar y tiempo de la encuesta, en grados centigrados
# autos: la cantidad de kilometros recorridos en auto (durante la ultima semana).
# cigarrillos: la cantidad de cigarrillos consumidos (durante la ultima semana)

# se ve qe hay datos erroneos edad -44 o 280 -> filtro las filas por edades de 5 a 100 años
#Internet2013 = Internet2013[(Internet2013$Nacionalidad == "URUGUAY"),]
Internet2013 = Internet2013[(Internet2013$Edad >= 5),]
Internet2013 = Internet2013[(Internet2013$Edad <= 100),]
# uso no puede ser negativo -> filtro por > 0
Internet2013 = Internet2013[(Internet2013$Uso >= 0),]
# temperatura no puede ser mayor de 50 grados centigrados -> filtro por temp > 50
Internet2013 = Internet2013[(Internet2013$Temperatura <= 50),]
#remover seco desconocido
Internet2013 = Internet2013[(Internet2013$Sexo != 0 ),]
Internet2013 = Internet2013[(Internet2013$Nacionalidad != "URUGUAY" ),]

########torta sexo, g1
sexo <- table(Internet2013$Sexo)
pct<-round(sexo/sum(sexo)*100) # calcula las frecuencias porcentuales
etiquetas <- c("Hombre","Mujer")
etiquetas <- paste(etiquetas, pct,"%")
pie3D(sexo, col=c("gray","blue","pink"), labels = etiquetas,font=8,cex=1.5,radius=1,border=TRUE,main="Encuestados por Sexo", init.angle=-90, angle=90,explode=0.1) # pone nombre


########torta pais
pais <- table(Internet2013$Nacionalidad)
etiquetas = names(pais)
pct<-round(pais/sum(pais)*100) # calcula las frecuencias porcentuales
etiquetas <- paste(etiquetas, pct,"%")
pie3D(pais, col=c("lightblue","green","white","black"),labels = etiquetas ,labelcex = 0.9,font=12,cex=1.5,radius=1,border=TRUE,main="Encuestados por Nacionalidad", init.angle=20, angle=90, explode=0.1) # pone nombre

########Fumadores ligeros (menos de 5 cigarrillos por dia) g2
#clasificacion: http://www.scielo.org.co/scielo.php?script=sci_arttext&pid=S1794-99982011000200007
fumador = Internet2013$Cigarrillos
fumador <- replace(fumador, strtoi(fumador) == 0, "NF")
fumador <- replace(fumador, strtoi(fumador) <= 5, "Leve")
fumador <- replace(fumador, (strtoi(fumador) <= 15), "Moderado")
fumador <- replace(fumador, (strtoi(fumador) < 999), "Severo")
t = table(fumador)
t = t[c(3,1,2,4)]
barplot(t,col=c("green","yellow","orange","red"), main="Categorías de Fumadores")

########g3
fumador = data.frame(Internet2013$Sexo, Internet2013$Cigarrillos)
colnames(fumador) <- c("Sexo", "Cigarrillos")
fumador$Sexo[fumador$Sexo == 1] = "Hombre"
fumador$Sexo[fumador$Sexo == 2] = "Mujer"

fumador$Cigarrillos = strtoi(fumador$Cigarrillos)

fumador$Cigarrillos[strtoi(fumador$Cigarrillos) == 0] = "NF"
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) <= 5] = "Leve"
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) <= 15] = "Moderado"
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) < 9999] = "Severo"

t = table(fumador)
t = t[,c(3,1,2,4)]
#names(t) = c ("NF", Leve","Mod", "Sev")
barplot(t,col=c("blue","pink"), main="Categorías de Fumadores")
legend("topright",cex=1,title="Sexo",c("M","F"),fill=c("blue","pink"),horiz=T) # asigna leyendas en posici?n horizontal


########g4
fumador = data.frame(Internet2013$Nacionalidad, Internet2013$Cigarrillos)
colnames(fumador) <- c("Pais", "Cigarrillos")


fumador$Cigarrillos = strtoi(fumador$Cigarrillos)
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) == 0] = "NF"
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) <= 5] = "Leve"
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) <= 15] = "M"
fumador$Cigarrillos[strtoi(fumador$Cigarrillos) < 9999] = "S"

t = table(fumador)
t = t[,c(4,2,1,3)]
t
colores = c("red","orange","yellow","green")
mosaicplot(t,col=colores,main="Categoria fumador x pais",ylab="Categoría de Fumadores",xlab="Pais",cex=0.8) 
# este gr?fico permite visualizar una tabla de contingencia

########g5

cig.muj=Internet2013$Cigarrillos[Internet2013$Sexo == 2] # guarda los datos de circunferencia de cintura de las mujeres
cig.var=Internet2013$Cigarrillos[Internet2013$Sexo == 1] # guarda los datos de circunferencia de cintura de los varones
qqplot(cig.muj,cig.var,col=8:9,pch=18,main="Comparación de distribuciones por sexo",xlab="Cigarrillos Mujeres",ylab="Cigarrillos Hombres")


autos.muj=Internet2013$Autos[Internet2013$Sexo == 2] # guarda los datos de circunferencia de cintura de las mujeres
autos.var=Internet2013$Autos[Internet2013$Sexo == 1] # guarda los datos de circunferencia de cintura de los varones
qqplot(autos.muj,autos.var,col=8:9,pch=18,main="Comparación de distribuciones por sexo",xlab="km en auto Mujeres",ylab="km en auto Hombres")


########g6
# estatura media por edad
minutos = Internet2013$Uso
hist(minutos,col="maroon1")

sitios = Internet2013$Sitio
hist(sitios,col="maroon1")

dev.off()