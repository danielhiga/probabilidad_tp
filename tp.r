setwd("/home/daniel/Documentos/utn/probabilidad_aplicada/tp/") # fija la ruta de la carpeta donde se encuentran los archivos con los que se va a trabajar y donde se guardarán las salidas que se ejecuten 
getwd() # devuelve la ruta de la carpeta de trabajo

library("xlsx")
library(readxl)
Internet2013 <- read_excel("Internet2013.xlsx")
#View(Internet2013)

campo = c("id", "nacionalidad", "edad", "estatura", "sitio", "uso", "temperatura", "autos", "cigarrillos")
descripcion_campo = c("id", "nacionalidad del encuestado", "edad del encuestado", "estatura del encuestado", "sitios web visitados por el usuario en la ultima semana", "minutos conectado a internet en la última semana", "temperatura ambiente al momento de la encuesta", "kilometros recorridos en automovil la ultima semana", "cigarrillos fumados la última semana")
info = data.frame(campo, descripcion_campo)
sal.info = info
# significado de los datos
# encuesta a usuarios de distintos paises sobre el uso de internet, autos y el consumo de tabaco 
# durante la semana anterior a la encuesta.
# id: id de registro
# Nacionalidad: pais de nacimiento del usuario
# edad: edad del usuario, en años
# estatura: estatura en metros del usuario
# sitio: la cantidad de sitios navegados (durante la ultima semana)
# uso: tiempo en minutos dedicado a navegar por internet (durante la ultima semana)
# temperatura: temperatura en el lugar y tiempo de la encuesta, en grados centigrados
# autos: la cantidad de kilometros recorridos en auto (durante la ultima semana).
# cigarrillos: la cantidad de cigarrillos consumidos (durante la ultima semana)

resumenOriginal = summary(Internet2013[c(3,5,6,7,8,9,10)])
resumenOriginal
sal.resunenOriginal = resumenOriginal
# se ve qe hay datos erroneos edad -44 o 280 -> filtro las filas por edades de 5 a 100 años
Internet2013 = Internet2013[(Internet2013$Edad >= 5),]
Internet2013 = Internet2013[(Internet2013$Edad <= 100),]
# uso no puede ser negativo -> filtro por > 0
Internet2013 = Internet2013[(Internet2013$Uso >= 0),]
# temperatura no puede ser mayor de 50 grados centigrados -> filtro por temp > 50
Internet2013 = Internet2013[(Internet2013$Temperatura <= 50),]


resumen1 = Internet2013[c(3,5,6,7,8,9,10)]
matrix(unlist(lapply(resumen1,"summary")),nrow=7,ncol=6,byrow=T)
r1 = matrix(unlist(lapply(resumen1,"summary")),nrow=7,ncol=6,byrow=T)
sd.r1=unlist(lapply(resumen1,"sd"))
r2=cbind(r1[,4],sd.r1,r1[,c(3,2,5)])
colnames(r2)<-c("MEDIA","SD","MEDIANA","Q1","Q3") # pone nombre a las columnas
r2 = round(r2, 2)
sal.resumen = r2

#edad estatura sitios uso cigarrillos promedio por pais
edadpromedioporpais <- aggregate(Internet2013$Edad, by=list(pais=Internet2013$Nacionalidad), FUN=mean)
estaturapromedioporpais <- aggregate(Internet2013$Estatura , by=list(pais=Internet2013$Nacionalidad), FUN=mean)
sitiospromedioporpais <- aggregate(Internet2013$Sitio , by=list(pais=Internet2013$Nacionalidad), FUN=mean)
usopromedioporpais <- aggregate(Internet2013$Uso , by=list(pais=Internet2013$Nacionalidad), FUN=mean)
temperaturapromedioporpais <- aggregate(Internet2013$Temperatura , by=list(pais=Internet2013$Nacionalidad), FUN=mean)
autospromedioporpais <- aggregate(Internet2013$Autos , by=list(pais=Internet2013$Nacionalidad), FUN=mean)
cigarrillospromedioporpais <- aggregate(Internet2013$Cigarrillos , by=list(pais=Internet2013$Nacionalidad), FUN=mean)
cantidaddatosporpais <- aggregate(Internet2013$Nacionalidad, by=list(pais=Internet2013$Nacionalidad), FUN=length)

tabla = edadpromedioporpais
rownames(tabla) = tabla$pais
tabla["estatura"] = estaturapromedioporpais$x
tabla["sitios"] = sitiospromedioporpais$x
tabla["uso"] = usopromedioporpais$x
tabla["temperatura"] = temperaturapromedioporpais$x
tabla["autos"] = autospromedioporpais$x
tabla["cigarrillos"] = cigarrillospromedioporpais$x
tabla["cantidad"] = cantidaddatosporpais$x
tabla$pais <- NULL
colnames(tabla) = c("edad", "estatura", "sitios", "uso", "temperatura", "autos", "cigarrillos", "cantidad")
tabla["edad"] = floor(tabla["edad"])
tabla["estatura"] = round(tabla["estatura"],2)
tabla["sitios"] = round(tabla["sitios"],2)
tabla["uso"] = round(tabla["uso"],2)
tabla["temperatura"] = round(tabla["temperatura"],2)
tabla["autos"] = round(tabla["autos"],2)
tabla["cigarrillos"] = round(tabla["cigarrillos"],2)
tabla = tabla[-4,]
print("media por pais")
tabla
sal.promediosporpais = tabla

######media mediana varianza des.estandar

base = Internet2013[c(3,5,6,7,8,9,10)]
maximo=round(apply(base,2,max),2) # calcula las minimo
minimo=round(apply(base,2,min),2) # calcula las maximo
media=round(apply(base,2,mean),2) # calcula las medias
mediana=round(apply(base,2,median),2) # calcula las medianas
varianza=round(apply(base,2,var),2) # calcula las varianzas
desv.standard=round(apply(base,2,sd),2) # calcula los desv?os est?ndar
resumenes=rbind(minimo, maximo,media,mediana,varianza,desv.standard) # junta en una tabla los res?menes
resumenes # muestra la salida
sal.resumen2 = resumenes
######
#edad estatura sitio usos temperatura auto cigarrillos promedio por sexo
edadpromedioporsexo <- aggregate(Internet2013$Edad, by=list(sexo=Internet2013$Sexo), FUN=mean)
estaturapromedioporsexo <- aggregate(Internet2013$Estatura , by=list(sexo=Internet2013$Sexo), FUN=mean)
sitiospromedioporsexo <- aggregate(Internet2013$Sitio, by=list(sexo=Internet2013$Sexo), FUN=mean)
usospromedioporsexo <- aggregate(Internet2013$Uso, by=list(sexo=Internet2013$Sexo), FUN=mean)
temperaturapromedioporsexo <- aggregate(Internet2013$Temperatura, by=list(sexo=Internet2013$Sexo), FUN=mean)
autospromedioporsexo <- aggregate(Internet2013$Autos , by=list(sexo=Internet2013$Sexo), FUN=mean)
cigarrillospromedioporsexo <- aggregate(Internet2013$Cigarrillos , by=list(sexo=Internet2013$Sexo), FUN=mean)
cantidaddatosporsexo <- aggregate(Internet2013$Nacionalidad, by=list(sexo=Internet2013$Sexo), FUN=length)

tablaporsexo = edadpromedioporsexo
tablaporsexo["estatura"] = estaturapromedioporsexo$x
tablaporsexo["sitios"] = sitiospromedioporsexo$x
tablaporsexo["usos"] = usospromedioporsexo$x
tablaporsexo["temperatura"] = temperaturapromedioporsexo$x
tablaporsexo["autos"] = autospromedioporsexo$x
tablaporsexo["cigarrillos"] = cigarrillospromedioporsexo$x
tablaporsexo["cantidad"] = cantidaddatosporsexo$x
tablaporsexo = tablaporsexo[-1,]
rownames(tablaporsexo) = c("Hombre", "Mujer")
tablaporsexo$sexo <- NULL
colnames(tablaporsexo) = c("edad","estatura","sitos","uso","temperatura","autos","cigarrillos", "cantidad")
tablaporsexo[,c(2,3,4,5,6,7)] <- round(tablaporsexo[,c(2,3,4,5,6,7)], 2)
tablaporsexo[,c(1)] <- round(tablaporsexo[,c(1)], 0)
print("promedio por sexo")
sal.promedioporsexo = tablaporsexo
tablaporsexo

##### resumen
base.split=split(Internet2013[,c(3,5,6,7,8,9,10)],Internet2013$Sexo) # arma una base numerica dividida seg?n el sexo
nosabe = as.data.frame(base.split[[1]])
hombres = as.data.frame(base.split[[2]])
mujeres = as.data.frame(base.split[[3]])


lapply(mujeres,"summary")
unlist(lapply(mujeres,"summary")) # transforma la salida de lista a vector
matrix(unlist(lapply(mujeres,"summary")),nrow=7,ncol=6,byrow=T) # acomoda los elementos del vector en una matriz de modo tal que cada variable ocupe una fila
sumary.muj=matrix(unlist(lapply(mujeres,"summary")),nrow=7,ncol=6,byrow=T) # guarda los res?menes estad?sticos
sd.mujeres=round(unlist(lapply(mujeres,"sd")),2)  # calcula el desv?o est?ndar de las variables de inter?s redondeado a dos d?gitos decimales
sal.muj=cbind(sumary.muj[,4],sd.mujeres,sumary.muj[,c(3,2,5)]) # combina las columnas indicadas
colnames(sal.muj)<-c("MEDIA","SD","MEDIANA","Q1","Q3") # pone nombre a las columnas
rownames(sal.muj)<-c("edad","estatura","sitios","uso","autos","cigarrillos") # pone nombre a las columnas
sal.muj = round(sal.muj,2)
sal.muj # devuelve la salida


lapply(hombres,"summary")
unlist(lapply(hombres,"summary")) # transforma la salida de lista a vector
matrix(unlist(lapply(hombres,"summary")),nrow=7,ncol=6,byrow=T) # acomoda los elementos del vector en una matriz de modo tal que cada variable ocupe una fila
sumary.hom=matrix(unlist(lapply(hombres,"summary")),nrow=7,ncol=6,byrow=T) # guarda los res?menes estad?sticos
sd.hombres=unlist(lapply(hombres,"sd"))  # calcula el desv?o est?ndar de las variables de inter?s redondeado a dos d?gitos decimales
sal.hom=cbind(sumary.hom[,4],sd.hombres,sumary.hom[,c(3,2,5)]) # combina las columnas indicadas
colnames(sal.hom)<-c("MEDIA","SD","MEDIANA","Q1","Q3") # pone nombre a las columnas
rownames(sal.hom)<-c("edad","estatura","sitios","uso","temperatura","autos","cigarrillos") # pone nombre a las columnas
sal.hom = round(sal.hom,2)
sal.hom # devuelve la salida



###### write to file
write.xlsx(sal.info, "out.xls", sheetName = "Info", col.names = TRUE, row.names = FALSE, append = FALSE)
write.xlsx(sal.resumen, "out.xls", sheetName = "Resumen", col.names = TRUE, row.names = TRUE, append = TRUE)
write.xlsx(sal.resumen2, "out.xls", sheetName = "media,mediana,varizanza,sd,max,min", col.names = TRUE, row.names = TRUE, append = TRUE)
write.xlsx(sal.muj, "out.xls", sheetName = "Resumen Mujeres", col.names = TRUE, row.names = TRUE, append = TRUE)
write.xlsx(sal.hom, "out.xls", sheetName = "Resumen Hombres", col.names = TRUE, row.names = TRUE, append = TRUE)
write.xlsx2(sal.promediosporpais, "out.xls", sheetName = "Media por Pais", col.names = TRUE, row.names = TRUE, append = TRUE)
write.xlsx2(sal.promedioporsexo, "out.xls", sheetName = "Media por Sexo", col.names = TRUE, row.names = TRUE, append = TRUE)
#write.xlsx2(sal., "out.xls", sheetName = "Resumen2", col.names = TRUE, row.names = TRUE, append = TRUE)

