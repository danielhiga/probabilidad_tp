#tp3 Daniel Higa
#Simulacion
setwd("/home/daniel/Documentos/utn/probabilidad_aplicada/tp/") # fija la ruta de la carpeta donde se encuentran los archivos con los que se va a trabajar y donde se guardarán las salidas que se ejecuten 
getwd() # devuelve la ruta de la carpeta de trabajo

#ruleta
#
fichas = 10000
maxtiradas = 100000
maxfichas = 10
(z <- Sys.time())
timestamp = unclass(z)
set.seed(timestamp)

numeros = 0:36
rojos = c(1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36)
negros = c(2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35)
pleno = 35

length(rojos)
length(negros)
aciertosPleno = 0
fallosPleno = 0

simulacion = function(fichas,maxtiradas, maxfichas) {
  
}

for(i in 1:maxtiradas){
  if (fichas > 0) {
    maxapuesta = min(maxfichas, fichas)
    apuesta = floor(runif(1,1,maxapuesta))
    fichas = fichas - apuesta
    numeroApostado = floor(runif(1,0,37))
    numeroGanador = floor(runif(1,0,37))
    if (numeroApostado == numeroGanador) {
      fichas = fichas + (apuesta * 35)
      print("acerto pleno numero")
      print(numeroGanador)
      print(" en iteracion")
      print(i)
      aciertosPleno = aciertosPleno + 1
    } else {
      print("perdio, fichas restantes")
      print(fichas)
      fallosPleno = fallosPleno + 1
    }
    if (fichas <= 0) {
       print("se quedo sin fichas")
    }
  }
}
fichas 
print("ratio aciertos / fallos")
print(aciertosPleno/fallosPleno)
print("probabilidad 1/37 = ")
print(1/37)